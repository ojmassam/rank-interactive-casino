package com.newcasino.CasinoSlotManager.model;

import com.newcasino.CasinoSlotManager.repository.dao.Transaction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

public class TransactionHistoryDto implements Serializable {
    private static final long serialVersionUID = 963515103009434789L;

    private String transactionId;
    private Long playerId;
    private String transactionTypeIdentifier;
    private BigDecimal amount;
    private BigDecimal amountPromotionsApplied;
    private PromotionDto promotionDto;
    private LocalDateTime transactionDateTime;

    public TransactionHistoryDto(final Transaction transaction) {
        this.transactionId = transaction.getTransactionId();
        this.playerId = transaction.getPlayer().getPlayerId();
        this.transactionTypeIdentifier = transaction.getTransactionTypeIdentifier();
        this.amount = transaction.getAmount();
        this.amountPromotionsApplied = transaction.getAmountPromotionsApplied();
        this.promotionDto = new PromotionDto(transaction.getPromotion());
        this.transactionDateTime = transaction.getTransactionDateTime();
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(final String transactionId) {
        this.transactionId = transactionId;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(final Long playerId) {
        this.playerId = playerId;
    }

    public String getTransactionTypeIdentifier() {
        return transactionTypeIdentifier;
    }

    public void setTransactionTypeIdentifier(final String transactionTypeIdentifier) {
        this.transactionTypeIdentifier = transactionTypeIdentifier;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmountPromotionsApplied() {
        return amountPromotionsApplied;
    }

    public void setAmountPromotionsApplied(final BigDecimal amountPromotionsApplied) {
        this.amountPromotionsApplied = amountPromotionsApplied;
    }

    public PromotionDto getPromotionDto() {
        return promotionDto;
    }

    public void setPromotionDto(final PromotionDto promotionDto) {
        this.promotionDto = promotionDto;
    }

    public LocalDateTime getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(final LocalDateTime transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionHistoryDto that = (TransactionHistoryDto) o;
        return Objects.equals(transactionId, that.transactionId) && Objects.equals(playerId, that.playerId) && Objects.equals(transactionTypeIdentifier, that.transactionTypeIdentifier) && Objects.equals(amount, that.amount) && Objects.equals(amountPromotionsApplied, that.amountPromotionsApplied) && Objects.equals(promotionDto, that.promotionDto) && Objects.equals(transactionDateTime, that.transactionDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, playerId, transactionTypeIdentifier, amount, amountPromotionsApplied, promotionDto, transactionDateTime);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", TransactionHistoryDto.class.getSimpleName() + "[", "]")
                .add("transactionId='" + transactionId + "'")
                .add("playerId=" + playerId)
                .add("transactionTypeIdentifier='" + transactionTypeIdentifier + "'")
                .add("amount=" + amount)
                .add("amountPromotionsApplied=" + amountPromotionsApplied)
                .add("discountDto=" + promotionDto)
                .add("transactionDateTime=" + transactionDateTime)
                .toString();
    }
}
