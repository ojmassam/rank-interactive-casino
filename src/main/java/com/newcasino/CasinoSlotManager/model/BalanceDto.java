package com.newcasino.CasinoSlotManager.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.newcasino.CasinoSlotManager.repository.dao.Player;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

public class BalanceDto implements Serializable {

    private static final long serialVersionUID = -8180340511913705579L;

    private Long playerId;
    private BigDecimal balance;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime transactionLocalDateTime;

    public BalanceDto(final LocalDateTime transactionLocalDateTime, final Player balance) {
        if (null != balance) {
            this.playerId = balance.getPlayerId();
            this.balance = balance.getAmount();
        }
        this.transactionLocalDateTime = transactionLocalDateTime;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(final Long playerId) {
        this.playerId = playerId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(final BigDecimal balance) {
        this.balance = balance;
    }

    public LocalDateTime getTransactionLocalDateTime() {
        return transactionLocalDateTime;
    }

    public void setTransactionLocalDateTime(final LocalDateTime transactionLocalDateTime) {
        this.transactionLocalDateTime = transactionLocalDateTime;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BalanceDto that = (BalanceDto) o;
        return Objects.equals(playerId, that.playerId) && Objects.equals(balance, that.balance) && Objects.equals(transactionLocalDateTime, that.transactionLocalDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, balance, transactionLocalDateTime);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BalanceDto.class.getSimpleName() + "[", "]")
                .add("playerId=" + playerId)
                .add("balance=" + balance)
                .add("transactionLocalDateTime=" + transactionLocalDateTime)
                .toString();
    }
}
