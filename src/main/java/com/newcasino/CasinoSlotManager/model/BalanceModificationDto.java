package com.newcasino.CasinoSlotManager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.newcasino.CasinoSlotManager.enums.TransactionTypeEnum;
import com.newcasino.CasinoSlotManager.repository.dao.Player;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BalanceModificationDto implements Serializable {

    private static final long serialVersionUID = 7160876566372348597L;

    private long playerId;
    private BigDecimal previousBalance;
    private BigDecimal newBalance;
    private TransactionTypeEnum transactionTypeEnum;
    private BigDecimal amount;
    private BigDecimal amountPromotionsApplied;
    private PromotionDto promotionDto;
    private String transactionId;
    @JsonIgnore
    private Player balance;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime transactionLocalDateTime;

    public BalanceModificationDto(final long playerId, final String transactionId, final BigDecimal amount, final LocalDateTime transactionLocalDateTime, final TransactionTypeEnum transactionTypeEnum, final Player balance) {
        this.playerId = playerId;
        this.transactionLocalDateTime = transactionLocalDateTime;
        this.transactionId = transactionId;
        this.transactionTypeEnum = transactionTypeEnum;
        this.amountPromotionsApplied = amount;
        this.amount = amount;
        this.balance = balance;
        if (null != balance) this.previousBalance = balance.getAmount();
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(final long playerId) {
        this.playerId = playerId;
    }

    public Player getBalance() {
        return balance;
    }

    public void setBalance(final Player balance) {
        this.balance = balance;
    }

    public BigDecimal getPreviousBalance() {
        return previousBalance;
    }

    public void setPreviousBalance(final BigDecimal previousBalance) {
        this.previousBalance = previousBalance;
    }

    public BigDecimal getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(final BigDecimal newBalance) {
        this.newBalance = newBalance;
    }

    public TransactionTypeEnum getTransactionTypeEnum() {
        return transactionTypeEnum;
    }

    public void setTransactionTypeEnum(final TransactionTypeEnum transactionTypeEnum) {
        this.transactionTypeEnum = transactionTypeEnum;
    }

    public BigDecimal getAmountPromotionsApplied() {
        return amountPromotionsApplied;
    }

    public void setAmountPromotionsApplied(final BigDecimal amountPromotionsApplied) {
        this.amountPromotionsApplied = amountPromotionsApplied;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public PromotionDto getPromotionDto() {
        return promotionDto;
    }

    public void setPromotionDto(final PromotionDto promotionDto) {
        this.promotionDto = promotionDto;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(final String transactionId) {
        this.transactionId = transactionId;
    }

    public LocalDateTime getTransactionLocalDateTime() {
        return transactionLocalDateTime;
    }

    public void setTransactionLocalDateTime(final LocalDateTime transactionLocalDateTime) {
        this.transactionLocalDateTime = transactionLocalDateTime;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BalanceModificationDto that = (BalanceModificationDto) o;
        return playerId == that.playerId && Objects.equals(previousBalance, that.previousBalance) && Objects.equals(newBalance, that.newBalance) && transactionTypeEnum == that.transactionTypeEnum && Objects.equals(amount, that.amount) && Objects.equals(amountPromotionsApplied, that.amountPromotionsApplied) && Objects.equals(promotionDto, that.promotionDto) && Objects.equals(transactionId, that.transactionId) && Objects.equals(balance, that.balance) && Objects.equals(transactionLocalDateTime, that.transactionLocalDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, previousBalance, newBalance, transactionTypeEnum, amount, amountPromotionsApplied, promotionDto, transactionId, balance, transactionLocalDateTime);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BalanceModificationDto.class.getSimpleName() + "[", "]")
                .add("playerId=" + playerId)
                .add("previousBalance=" + previousBalance)
                .add("newBalance=" + newBalance)
                .add("transactionTypeEnum=" + transactionTypeEnum)
                .add("amount=" + amount)
                .add("amountWithPromotionsApplied=" + amountPromotionsApplied)
                .add("discount=" + promotionDto)
                .add("transactionId='" + transactionId + "'")
                .add("balance=" + balance)
                .add("transactionLocalDateTime=" + transactionLocalDateTime)
                .toString();
    }
}
