package com.newcasino.CasinoSlotManager.model;

import java.io.Serializable;

public class TransactionHistoryRequest implements Serializable {

    private static final long serialVersionUID = 8251438399218152852L;

    private String playerUsername;
    private String password;

    public String getPlayerUsername() {
        return playerUsername;
    }

    public void setPlayerUsername(final String playerUsername) {
        this.playerUsername = playerUsername;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
