package com.newcasino.CasinoSlotManager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.newcasino.CasinoSlotManager.repository.dao.Promotion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.StringJoiner;

public class PromotionDto implements Serializable {

    private static final long serialVersionUID = -9158109128399310399L;

    @JsonIgnore
    private Long promotionId;
    private String discountIdentifier;
    private BigDecimal discountPercent;
    @JsonIgnore
    private Boolean isLimited;
    @JsonIgnore
    private Long available;

    public PromotionDto(final Promotion discount) {
        this.promotionId = discount.getDiscountId();
        this.discountIdentifier = discount.getIdentifier();
        this.discountPercent = discount.getDiscountPercent();
        this.isLimited = discount.getLimited();
        this.available = discount.getAvailable();
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(final Long promotionId) {
        this.promotionId = promotionId;
    }

    public String getDiscountIdentifier() {
        return discountIdentifier;
    }

    public PromotionDto setDiscountIdentifier(final String discountIdentifier) {
        this.discountIdentifier = discountIdentifier;
        return this;
    }

    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }

    public PromotionDto setDiscountPercent(final BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
        return this;
    }

    public Boolean getLimited() {
        return isLimited;
    }

    public void setLimited(final Boolean limited) {
        isLimited = limited;
    }

    public Long getAvailable() {
        return available;
    }

    public void setAvailable(final Long available) {
        this.available = available;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PromotionDto that = (PromotionDto) o;
        return Objects.equals(promotionId, that.promotionId) && Objects.equals(discountIdentifier, that.discountIdentifier) && Objects.equals(discountPercent, that.discountPercent) && Objects.equals(isLimited, that.isLimited) && Objects.equals(available, that.available);
    }

    @Override
    public int hashCode() {
        return Objects.hash(promotionId, discountIdentifier, discountPercent, isLimited, available);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PromotionDto.class.getSimpleName() + "[", "]")
                .add("promotionId=" + promotionId)
                .add("discountIdentifier='" + discountIdentifier + "'")
                .add("discountPercent=" + discountPercent)
                .add("isLimited=" + isLimited)
                .add("available=" + available)
                .toString();
    }
}
