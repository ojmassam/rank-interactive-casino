package com.newcasino.CasinoSlotManager.controller;

import com.newcasino.CasinoSlotManager.exception.CasinoException;
import com.newcasino.CasinoSlotManager.model.BalanceDto;
import com.newcasino.CasinoSlotManager.model.BalanceModificationDto;
import com.newcasino.CasinoSlotManager.model.TransactionHistoryDto;
import com.newcasino.CasinoSlotManager.model.TransactionHistoryRequest;
import com.newcasino.CasinoSlotManager.service.ICasinoService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/casino")
public class CasinoController {

    @Autowired
    private ICasinoService casinoService;

    @GetMapping("/player/balance")
    public BalanceDto getPlayerBalance(
            @ApiParam(value = "The player ID", name = "playerId", required = true)
            @RequestParam(value = "playerId") final Long playerId){

        BalanceDto balanceDto = casinoService.getPlayerBalance(playerId);

        return balanceDto;
    }

    @PostMapping("/player/credit")
    public BalanceModificationDto creditPlayer(
            @ApiParam(value = "The player ID", name = "playerId", required = true)
            @RequestParam(value = "playerId") final Long playerId,
            @ApiParam(value = "The amount to be credited", name = "amount", required = true)
            @RequestParam(value = "amount") final BigDecimal amount,
            @ApiParam(value = "The transaction ID", name = "transactionId", required = true)
            @RequestParam(value = "transactionId") final String transactionId){

        BalanceModificationDto balanceModificationDto = casinoService.creditPlayer(playerId, transactionId, amount);

        return balanceModificationDto;
    }

    @PostMapping("/player/debit")
    public BalanceModificationDto debitPlayer(
            @ApiParam(value = "The player ID", name = "playerId", required = true)
            @RequestParam(value = "playerId") final Long playerId,
            @ApiParam(value = "The amount to be debited", name = "amount", required = true)
            @RequestParam(value = "amount") final BigDecimal amount,
            @ApiParam(value = "The transaction ID", name = "transactionId", required = true)
            @RequestParam(value = "transactionId") final String transactionId,
            @ApiParam(value = "The promotion code", name = "promotionCode", required = false)
            @RequestParam(value = "promotionCode", required = false) final String promotionCode){

        BalanceModificationDto balanceModificationDto = casinoService.debitPlayer(playerId, transactionId, amount, promotionCode);

        return balanceModificationDto;
    }

    @PostMapping("/player/transaction-history")
    public ResponseEntity<List<TransactionHistoryDto>> getTransactionHistory(
            @ApiParam(value = "Transaction history request body.",
                    name = "transactionHistoryRequest",
                    required = true)
            @RequestBody TransactionHistoryRequest transactionHistoryRequest){

        List<TransactionHistoryDto> transactionHistoryDtoList = casinoService.getPlayerTransactionHistory(transactionHistoryRequest);

        return new ResponseEntity<>(transactionHistoryDtoList, HttpStatus.OK);
    }
}
