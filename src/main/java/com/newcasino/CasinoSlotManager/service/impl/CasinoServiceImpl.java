package com.newcasino.CasinoSlotManager.service.impl;

import com.newcasino.CasinoSlotManager.enums.CasinoErrorEnum;
import com.newcasino.CasinoSlotManager.enums.TransactionTypeEnum;
import com.newcasino.CasinoSlotManager.exception.CasinoException;
import com.newcasino.CasinoSlotManager.model.BalanceDto;
import com.newcasino.CasinoSlotManager.model.BalanceModificationDto;
import com.newcasino.CasinoSlotManager.model.PromotionDto;
import com.newcasino.CasinoSlotManager.model.TransactionHistoryDto;
import com.newcasino.CasinoSlotManager.model.TransactionHistoryRequest;
import com.newcasino.CasinoSlotManager.repository.PlayerRepository;
import com.newcasino.CasinoSlotManager.repository.PromotionRepository;
import com.newcasino.CasinoSlotManager.repository.TransactionRepository;
import com.newcasino.CasinoSlotManager.repository.dao.Player;
import com.newcasino.CasinoSlotManager.repository.dao.Promotion;
import com.newcasino.CasinoSlotManager.repository.dao.Transaction;
import com.newcasino.CasinoSlotManager.service.ICasinoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CasinoServiceImpl implements ICasinoService {

    // Added some constants to reduce some coding time
    private static final String DEFAULT_PROMOTION = "NONE";
    private static final String TRANSACTION_HISTORY_PASSWORD = "swordfish";
    private static final int TRANSACTION_HISTORY_RESPONSE_LIMIT = 10;
    private static final Long PROMOTION_AVAILABILITY = 5L;
    private static String ACTIVE_PROMOTION = "NONE";

    private final PlayerRepository playerRepository;
    private final TransactionRepository transactionRepository;
    private final PromotionRepository promotionRepository;

    @Autowired
    public CasinoServiceImpl(final PlayerRepository playerRepository, final TransactionRepository transactionRepository, final PromotionRepository promotionRepository) {
        this.playerRepository = playerRepository;
        this.transactionRepository = transactionRepository;
        this.promotionRepository = promotionRepository;
    }

    @Override
    public BalanceModificationDto debitPlayer(final long playerId, final String transactionId, final BigDecimal amount, final String promotionCode) {
        validateTransactionId(transactionId);
        BalanceModificationDto balanceModificationDto = new BalanceModificationDto(playerId,
                transactionId,
                amount,
                LocalDateTime.now(),
                TransactionTypeEnum.DEBIT,
                getPlayer(playerId));

        if (null != promotionCode) {
            updatePromotionAvailability(promotionCode, PROMOTION_AVAILABILITY);
        }

        balanceModificationDto.setPromotionDto(new PromotionDto(determineDebitPromotion()));

        return modifyPlayerBalance(balanceModificationDto);
    }

    @Override
    public BalanceModificationDto creditPlayer(final long playerId, final String transactionId, final BigDecimal amount) {
        validateTransactionId(transactionId);
        BalanceModificationDto balanceModificationDto = new BalanceModificationDto(playerId,
                transactionId,
                amount,
                LocalDateTime.now(),
                TransactionTypeEnum.CREDIT,
                getPlayer(playerId));

        balanceModificationDto.setPromotionDto(new PromotionDto(determineCreditPromotion()));

        return modifyPlayerBalance(balanceModificationDto);
    }

    @Override
    public BalanceDto getPlayerBalance(final long playerId) {
        return new BalanceDto(LocalDateTime.now(), getPlayer(playerId));
    }

    @Override
    public List<TransactionHistoryDto> getPlayerTransactionHistory(final TransactionHistoryRequest transactionHistoryRequest) {
        validatePassword(transactionHistoryRequest.getPassword());

        Player player = playerRepository.findByUsername(transactionHistoryRequest.getPlayerUsername());
        if (null == player) throw new CasinoException(CasinoErrorEnum.INVALID_USER);

        List<Transaction> transactionList = player.getTransactionList();
        List<TransactionHistoryDto> transactionHistoryDtoList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(transactionList)) {
            for (int i = 0; i < Math.min(TRANSACTION_HISTORY_RESPONSE_LIMIT, transactionList.size()); i++) {
                transactionHistoryDtoList.add(new TransactionHistoryDto(transactionList.get(i)));
            }
        }
        return transactionHistoryDtoList;
    }

    private void validatePassword(final String password) {
        if (!TRANSACTION_HISTORY_PASSWORD.equals(password)) {
            throw new CasinoException(CasinoErrorEnum.INVALID_PASSWORD);
        }
    }

    private void validateTransactionId(final String transactionId) {
        Transaction transaction = transactionRepository.getTransactionByTransactionId(transactionId);
        if (null != transaction) {
            throw new CasinoException(CasinoErrorEnum.INVALID_TRANSACTION_ID);
        }
    }

    private Player getPlayer(final long playerId) {
        Player player = playerRepository.findByPlayerId(playerId);
        if (null == player) {
            throw new CasinoException(CasinoErrorEnum.INVALID_USER);
        }
        return player;
    }

    private Promotion determineDebitPromotion() {
        Promotion promotion = promotionRepository.getPromotionByIdentifier(ACTIVE_PROMOTION);
        if (null != promotion && promotion.getLimited() && promotion.getAvailable() > 0) {
            promotion.setAvailable(promotion.getAvailable() - 1);
            promotionRepository.save(promotion);
        } else if (null == promotion || promotion.getLimited() && promotion.getAvailable() < 1) {
            ACTIVE_PROMOTION = DEFAULT_PROMOTION;
            promotion = promotionRepository.getPromotionByIdentifier(DEFAULT_PROMOTION);
        }
        return promotion;
    }

    private Promotion determineCreditPromotion() {
        return promotionRepository.getPromotionByIdentifier(DEFAULT_PROMOTION);
    }

    private BalanceModificationDto modifyPlayerBalance(BalanceModificationDto balanceModificationDto) {
        applyPromotions(balanceModificationDto);

        switch (balanceModificationDto.getTransactionTypeEnum()) {
            case DEBIT:
                balanceModificationDto.setNewBalance(balanceModificationDto.getBalance().getAmount().subtract(balanceModificationDto.getAmount().subtract(balanceModificationDto.getAmountPromotionsApplied())));
                if (balanceModificationDto.getNewBalance().compareTo(BigDecimal.ZERO) < 0) {
                    throw new CasinoException(CasinoErrorEnum.PLAYER_OUT_OF_FUNDS);
                }
                break;
            case CREDIT:
                balanceModificationDto.setNewBalance(balanceModificationDto.getBalance().getAmount().add(balanceModificationDto.getAmount().add(balanceModificationDto.getAmountPromotionsApplied())));
                break;
        }

        transactionRepository.save(buildTransaction(balanceModificationDto));
        return balanceModificationDto;
    }

    private void applyPromotions(BalanceModificationDto balanceModificationDto) {
        if (null != balanceModificationDto.getPromotionDto()) {
            BigDecimal promotionValue = balanceModificationDto.getAmount().multiply(balanceModificationDto.getPromotionDto().getDiscountPercent().divide(BigDecimal.valueOf(100)));
            balanceModificationDto.setAmountPromotionsApplied(promotionValue);
        }
    }

    private Transaction buildTransaction(BalanceModificationDto balanceModificationDto) {
        Transaction transaction = new Transaction();
        transaction.setTransactionId(balanceModificationDto.getTransactionId());
        transaction.setAmount(balanceModificationDto.getAmount());
        transaction.setTransactionDateTime(balanceModificationDto.getTransactionLocalDateTime());
        transaction.setTransactionTypeIdentifier(balanceModificationDto.getTransactionTypeEnum().name());
        transaction.setAmountPromotionsApplied(balanceModificationDto.getAmountPromotionsApplied());
        transaction.setPlayer(buildPlayer(balanceModificationDto));
        transaction.setPromotion(buildPromotion(balanceModificationDto));

        return transaction;
    }

    private Promotion buildPromotion(final BalanceModificationDto balanceModificationDto) {
        Promotion promotion = new Promotion();
        promotion.setDiscountId(balanceModificationDto.getPromotionDto().getPromotionId());
        promotion.setDiscountPercent(balanceModificationDto.getPromotionDto().getDiscountPercent());
        promotion.setIdentifier(balanceModificationDto.getPromotionDto().getDiscountIdentifier());
        return promotion;
    }

    private Player buildPlayer(final BalanceModificationDto balanceModificationDto) {
        Player balance = new Player();
        balance.setPlayerId(balanceModificationDto.getPlayerId());
        balance.setUsername(balanceModificationDto.getBalance().getUsername());
        balance.setAmount(balanceModificationDto.getNewBalance());
        return balance;
    }

    private void updatePromotionAvailability(String identifier, Long availability) {
        Promotion promotion = promotionRepository.getPromotionByIdentifier(identifier);
        if (null != promotion) {
            promotion.setAvailable(availability);
            ACTIVE_PROMOTION = identifier;
            promotionRepository.save(promotion);
        }
    }
}
