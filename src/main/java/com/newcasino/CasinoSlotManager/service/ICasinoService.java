package com.newcasino.CasinoSlotManager.service;

import com.newcasino.CasinoSlotManager.model.BalanceDto;
import com.newcasino.CasinoSlotManager.model.BalanceModificationDto;
import com.newcasino.CasinoSlotManager.model.TransactionHistoryDto;
import com.newcasino.CasinoSlotManager.model.TransactionHistoryRequest;

import java.math.BigDecimal;
import java.util.List;

public interface ICasinoService {
    /**
     * Deduct amount from player balance (Wager)
     *
     * @param playerId      unique player ID
     * @param transactionId unique transaction ID
     * @param amount        the value to be deducted from player balance
     * @param promotionCode an optional promotion code
     * @return player modified balance details
     */
    BalanceModificationDto debitPlayer(long playerId, String transactionId, BigDecimal amount, String promotionCode);

    /**
     * Add amount to player balance (Winning)
     *
     * @param playerId      unique player ID
     * @param transactionId unique transaction ID
     * @param amount        the value to be added to player balance
     * @return player modified balance details
     */
    BalanceModificationDto creditPlayer(long playerId, String transactionId, BigDecimal amount);

    /**
     * @param playerId unique player ID
     * @return player balance details
     */
    BalanceDto getPlayerBalance(long playerId);

    /**
     * Returns a players last 10 transactions (Debit and Credit)
     *
     *
     * @param transactionHistoryRequest @return player transaction history
     */
    List<TransactionHistoryDto> getPlayerTransactionHistory(TransactionHistoryRequest transactionHistoryRequest);
}
