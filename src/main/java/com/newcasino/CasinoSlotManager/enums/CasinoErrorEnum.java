package com.newcasino.CasinoSlotManager.enums;

import org.springframework.http.HttpStatus;

public enum CasinoErrorEnum {

    INVALID_USER("Invalid user provided", HttpStatus.BAD_REQUEST),
    INVALID_TRANSACTION_ID("Invalid transaction ID provided, an identical ID already exists", HttpStatus.BAD_REQUEST),
    PLAYER_OUT_OF_FUNDS("Player has run out of funds", HttpStatus.I_AM_A_TEAPOT),
    INVALID_PASSWORD("Incorrect Password!", HttpStatus.UNAUTHORIZED);

    private final String message;
    private final HttpStatus httpStatus;

    CasinoErrorEnum(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
