package com.newcasino.CasinoSlotManager.exception;

import com.newcasino.CasinoSlotManager.enums.CasinoErrorEnum;
import org.springframework.http.HttpStatus;

public class CasinoException extends RuntimeException{
    private static final long serialVersionUID = 6984785208399083507L;

    private final CasinoErrorEnum casinoErrorEnum;
    private final HttpStatus httpStatus;

    public CasinoException(CasinoErrorEnum casinoErrorEnum) {
        super(casinoErrorEnum.getMessage());
        this.casinoErrorEnum = casinoErrorEnum;
        this.httpStatus = casinoErrorEnum.getHttpStatus();
    }

    public CasinoErrorEnum getCasinoErrorEnum() {
        return casinoErrorEnum;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
