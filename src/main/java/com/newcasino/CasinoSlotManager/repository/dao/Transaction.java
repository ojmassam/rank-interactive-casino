package com.newcasino.CasinoSlotManager.repository.dao;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "Transaction")
public class Transaction {
    @Id
    @Column(name = "transaction_id", unique=true)
    private String transactionId;

    @Column(name = "transaction_type_identifier")
    private String transactionTypeIdentifier;

    @Column(name = "amount")
    private BigDecimal amount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "discount_id")
    private Promotion promotion;

    @Column(name = "amount_promotions_applied")
    private BigDecimal amountPromotionsApplied;

    @Column(name = "transaction_date_time")
    private LocalDateTime transactionDateTime;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "player_id")
    private Player player;

    public Transaction() {
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(final String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionTypeIdentifier() {
        return transactionTypeIdentifier;
    }

    public void setTransactionTypeIdentifier(final String transactionTypeIdentifier) {
        this.transactionTypeIdentifier = transactionTypeIdentifier;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(final Promotion promotion) {
        this.promotion = promotion;
    }

    public BigDecimal getAmountPromotionsApplied() {
        return amountPromotionsApplied;
    }

    public void setAmountPromotionsApplied(final BigDecimal discountAmount) {
        this.amountPromotionsApplied = discountAmount;
    }

    public LocalDateTime getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(final LocalDateTime transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(final Player balance) {
        this.player = balance;
    }

}
