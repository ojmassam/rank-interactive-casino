package com.newcasino.CasinoSlotManager.repository.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "Promotion")
public class Promotion {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "discount_id")
    private Long discountId;

    @Column(name = "identifier")
    private String identifier;

    @Column(name = "discount_percent")
    private BigDecimal discountPercent;

    @Column(name = "is_limited")
    private Boolean isLimited;

    @Column(name = "available")
    private Long available;

    public Promotion() {
    }

    public Long getDiscountId() {
        return discountId;
    }

    public void setDiscountId(final Long discountId) {
        this.discountId = discountId;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(final String identifier) {
        this.identifier = identifier;
    }

    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(final BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Boolean getLimited() {
        return isLimited;
    }

    public void setLimited(final Boolean limited) {
        isLimited = limited;
    }

    public Long getAvailable() {
        return available;
    }

    public void setAvailable(final Long available) {
        this.available = available;
    }
}
