package com.newcasino.CasinoSlotManager.repository;

import com.newcasino.CasinoSlotManager.repository.dao.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PromotionRepository extends JpaRepository<Promotion, Long> {
    Promotion getPromotionByIdentifier(final String identifier);
}
