package com.newcasino.CasinoSlotManager.repository;

import com.newcasino.CasinoSlotManager.repository.dao.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    Transaction getTransactionByTransactionId(final String transactionId);
}
