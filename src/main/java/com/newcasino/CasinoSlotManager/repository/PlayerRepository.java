package com.newcasino.CasinoSlotManager.repository;

import com.newcasino.CasinoSlotManager.repository.dao.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {
    Player findByPlayerId(final Long playerId);

    Player findByUsername(final String username);
}
