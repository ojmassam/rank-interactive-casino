# Casino Slot Manager #

### Base Endpoint ###
http://localhost:8080/casino-slot-manager/

### Swagger UI ###
http://localhost:8080/casino-slot-manager/swagger-ui.html

### H2 Memory DB ###
Connection config for console can be found in application properties file: 
http://localhost:8080/casino-slot-manager/h2-ui

## Project notes due to time contraints ##
- Needs to be refactored - MVP implemented
- The more niche config was added as constants and managed by global variables - This needs to be reworked into a proper solution in a more generic and configurable way.
- Unit tests ommited
- DB table design needs to be revisted in general
- More extensive testing required - covered vanilla cases only